# Dépôt pour le cours "Analyse des besoins" de l'EPSI en I1

## Membres du groupes
* Bréval LE FLOCH
* Andy CINQUIN
* Joris FLOHIC
* Mathis GAUTHIER

## Langage de programmation choisi
* GO [installation](https://go.dev/doc/install)

## Structure du projet :
L'ensemble du code de la machine à café se trouve dans le fichier ./coffee/coffee.go
Les tests de se trouve dans le fichier ./coffee/coffee_test.go
Le fichier main.go n'est qu'un hello world

## Run tests
pour lancer les tests, executer la commande suivante : 
```bash
go test ./coffee
```