package coffee

type CoffeeMachine struct {
	name              string
	coffeeDose        int
	goblet            int
	sugarDose         int
	sugarDoseInCoffee int
	water             bool
	moneyInserted     float64
	actualTotalMoney  float64
	cupDetected       bool
}

func (m *CoffeeMachine) SetName(name string) {
	m.name = name
}

func (m *CoffeeMachine) SetCoffeeDose(coffeeDose int) {
	m.coffeeDose = coffeeDose
}

func (m *CoffeeMachine) SetGoblet(goblet int) {
	m.goblet = goblet
}

func (m *CoffeeMachine) SetSugarDose(sugarDose int) {
	m.sugarDose = sugarDose
}

func (m *CoffeeMachine) SetSugarDoseInCoffee(sugarDoseInCoffee int) {
	m.sugarDoseInCoffee = sugarDoseInCoffee
}

func (m *CoffeeMachine) SetWater(water bool) {
	m.water = water
}

func (m *CoffeeMachine) SetMoneyInserted(moneyInserted float64) {
	m.moneyInserted = moneyInserted
}

func (m *CoffeeMachine) SetActualTotalMoney(actualTotalMoney float64) {
	m.actualTotalMoney = actualTotalMoney
}

func (m *CoffeeMachine) SetCupDetected(cupDetected bool) {
	m.cupDetected = cupDetected
}

func (m *CoffeeMachine) insertCoin(moneyToInsert float64) {
	m.moneyInserted += moneyToInsert
	m.actualTotalMoney += moneyToInsert

	m.makeCoffee()
}

func (m *CoffeeMachine) refund() {
	m.moneyInserted = 0
	m.actualTotalMoney -= 0.4
}

func (m *CoffeeMachine) addSugarDoseInCoffee() {
	m.sugarDoseInCoffee++
}
func (m *CoffeeMachine) hasGoblet() bool {
	return m.goblet > 0
}

func (m *CoffeeMachine) reloadCoffee(numberCoffeeDose int) {
	m.coffeeDose += numberCoffeeDose
	if m.coffeeDose > 60 {
		m.coffeeDose = 60
	}
}

func (m *CoffeeMachine) reloadGobelet(numberGobelet int) {
	m.goblet += numberGobelet
	if m.goblet > 100 {
		m.goblet = 100
	}
}

func (m *CoffeeMachine) makeCoffee() {
	if m.coffeeDose > 0 && (m.goblet > 0 || m.cupDetected) && m.water && m.moneyInserted >= 0.40 {
		m.coffeeDose--
		if !m.cupDetected {
			m.goblet--
		}
		if m.sugarDoseInCoffee > 0 {
			m.sugarDose -= m.sugarDoseInCoffee
		}
	} else {
		m.refund()
	}
}

func (m *CoffeeMachine) reloadGoblets(gobletsToAdd int) {
	m.goblet += gobletsToAdd
}
