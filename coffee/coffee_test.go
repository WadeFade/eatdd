package coffee

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

/*
QUAND on met 40cts
ALORS un café coule
*/
func Test_when_insert_coin_then_coffee_is_served(t *testing.T) {
	//init avec 10 gobelets et 10 doses de café
	var machine = CoffeeMachineBuilder()

	// Quand on met 40cts
	// ALORS un café coule
	baseMoney := machine.actualTotalMoney
	baseCoffeeDose := machine.coffeeDose

	machine.insertCoin(0.40)

	assert.Equal(t, machine.actualTotalMoney, baseMoney+0.40, "actualTotalMoney should be 0.40")

	//café bien couler
	assert.Equal(t, machine.coffeeDose, baseCoffeeDose-1, "coffeeDose should be 9")
}

/*
QUAND un café coule
ALORS on décrémente de 1 le nombre de doses de café
*/
func Test_when_coffee_is_served_minus_one_coffee_dose(t *testing.T) {
	var machine = CoffeeMachineBuilder()

	var baseCoffeeDose = machine.coffeeDose

	machine.insertCoin(0.40)
	// Alors le café coule

	assert.Equal(t, machine.coffeeDose, baseCoffeeDose-1, "coffeeDose should be 9")
}

/*
QUAND on met 40cts
ALORS on décrémente de 1 le nombre de gobelet
*/
func Test_when_coffee_is_served_minus_one_goblets(t *testing.T) {
	var machine = CoffeeMachineBuilder()

	var baseGoblets = machine.goblet
	// Alors le café coule
	machine.insertCoin(0.40)

	assert.Equal(t, machine.goblet, baseGoblets-1, "goblet should be 9")
}

/*
GIVEN - WHEN - Donner >40cts
THEN - Café coule ET on encaisse tout l'argent
*/
func Test_when_insert_more_coin_then_coffee_is_served(t *testing.T) {
	//init avec 10 gobelets et 10 doses de café
	var machine = CoffeeMachineBuilder()

	// Quand on met 50cts
	// Alors le café coule
	baseMoney := machine.actualTotalMoney
	baseCoffeeDose := machine.coffeeDose

	coinsHasInserted := 0.40 + 0.10 // x > 0.40

	machine.insertCoin(coinsHasInserted)

	assert.Equal(t, machine.actualTotalMoney, baseMoney+coinsHasInserted, "actualTotalMoney should be 0.50")

	//café bien couler
	assert.Equal(t, machine.coffeeDose, baseCoffeeDose-1, "coffeeDose should be 9")
}

/*
*
ETANT donne qu'il n'y a plus de gobelets ET qu'une tasse est détecté et
QUAND on met 40cts
ALORS le café coule
*/
func Test_when_no_goblet_and_goblet_detected_then_coffee_is_served(t *testing.T) {
	var machine = CoffeeMachineBuilder()
	machine.SetGoblet(0)
	machine.SetCupDetected(true)

	baseCoffeeDose := machine.coffeeDose
	// Quand on met 40cts
	machine.insertCoin(0.40)

	// Alors le café coule
	assert.Equal(t, machine.coffeeDose, baseCoffeeDose-1, "coffeeDose should be 9")
}

/*
*
ETANT donne qu'une tasse est détecté et
QUAND on met 40cts
ALORS le café coule
*/
func Test_when_goblet_detected_then_coffee_is_served(t *testing.T) {
	var machine = CoffeeMachineBuilder()
	machine.SetCupDetected(true)

	baseCoffeeDose := machine.coffeeDose
	baseGoblets := machine.goblet

	// Quand on met 40cts
	machine.insertCoin(0.40)

	// Alors le café coule
	assert.Equal(t, machine.coffeeDose, baseCoffeeDose-1, "coffeeDose should be 9")

	// Alors le gobelet n'est pas décrémenté
	assert.Equal(t, machine.goblet, baseGoblets, "goblet should be 10")
}

/*
*
ETANT DONNE qu'il n'y a plus de café
QUAND on met 40cts
ALORS l'argent est rendu
*/
func Test_when_insert_coin_and_no_coffee_then_coffee_isnt_served(t *testing.T) {
	var machine = CoffeeMachineBuilder()
	machine.SetCoffeeDose(0)

	// Quand on met 40cts
	// Alors le café coule
	baseMoney := machine.actualTotalMoney
	machine.insertCoin(0.40)

	assert.Equal(t, machine.actualTotalMoney, baseMoney, "actualTotalMoney should be 0.40")

	//café pas couler
	assert.Equal(t, machine.coffeeDose, 0, "coffeeDose should be 0")
}

/*
*
ETANT DONNE qu'il n'y plus de gobelets
QUAND on met 40cts
ALORS rend 40cts et pas de café
*/
func Test_when_insert_coin_and_no_goblet_then_coffee_isnt_served(t *testing.T) {
	var machine = CoffeeMachineBuilder()
	machine.SetGoblet(0)

	// Quand on met 40cts
	baseMoney := machine.actualTotalMoney
	baseCoffeeDose := machine.coffeeDose
	machine.insertCoin(0.40)

	assert.Equal(t, machine.actualTotalMoney, baseMoney, "actualTotalMoney should be 0.40")

	//café pas couler
	assert.Equal(t, machine.coffeeDose, baseCoffeeDose, "coffeeDose should be 0")

}

/*
*
Etant donné que la machine est hors service (plus d’eau)
Quand on met 40 centimes
Alors on rend la monnaie
*/
func Test_when_insert_coin_and_no_water_then_coffee_isnt_served(t *testing.T) {
	var machine = CoffeeMachineBuilder()
	machine.SetWater(false)

	// Quand on met 40cts
	baseMoney := machine.actualTotalMoney
	baseCoffeeDose := machine.coffeeDose
	machine.insertCoin(0.40)

	assert.Equal(t, machine.actualTotalMoney, baseMoney, "actualTotalMoney should be 0.40")

	//café pas couler
	assert.Equal(t, machine.coffeeDose, baseCoffeeDose, "coffeeDose should be 0")
}

/*
*
Etant donné que l'utilisateur a appuyé sur sucre
Quand l'utilisateur met 40 cts
Alors le café coule avec le sucre ajouté
*/
func Test_when_user_press_sugar_then_coffee_is_served_with_sugar(t *testing.T) {
	//init avec 10 gobelets et 10 doses de café et 10 sucre
	var machine = CoffeeMachineBuilder()

	//Etant donné que l'utilisateur a appuyé sur sucre
	machine.addSugarDoseInCoffee()

	// Quand on met 40cts
	// Alors le café coule
	baseSugarDose := machine.sugarDose
	baseCoffeeDose := machine.coffeeDose
	machine.insertCoin(0.40)

	assert.Equal(t, machine.sugarDose, baseSugarDose-1, "sugarDose should be 9")

	//café bien couler
	assert.Equal(t, machine.coffeeDose, baseCoffeeDose-1, "coffeeDose should be 9")
}

/*
Etant donné que l'utilisateur a appuyé sur sucre plusieurs fois
Quand l'utilisateur met 40 cts
Alors le café coule avec le sucre ajouté
*/
func Test_when_user_multi_press_sugar_then_coffee_is_served_with_sugar(t *testing.T) {
	//init avec 10 gobelets et 10 doses de café et 10 sucre
	var machine = CoffeeMachineBuilder()

	//Etant donné que l'utilisateur a appuyé sur sucre
	machine.addSugarDoseInCoffee()
	machine.addSugarDoseInCoffee()

	// Quand on met 40cts
	// Alors le café coule
	baseSugarDose := machine.sugarDose
	baseCoffeeDose := machine.coffeeDose
	machine.insertCoin(0.40)

	assert.Equal(t, machine.sugarDose, baseSugarDose-2, "sugarDose should be 9")

	//café bien couler
	assert.Equal(t, machine.coffeeDose, baseCoffeeDose-1, "coffeeDose should be 9")
}

/*
*
QUAND le technicien remet <x> café
ALORS on ajoute <x> stock de café
*/
func Test_when_coffee_recharge_then_add_it_to_stock(t *testing.T) {
	var machine = CoffeeMachineBuilder()
	machine.SetCoffeeDose(0)

	var baseBeans = machine.coffeeDose
	// When le technicien remet <x> café
	machine.reloadCoffee(2)

	machine.insertCoin(0.40) // la machine marche toujours

	assert.Equal(t, machine.coffeeDose, baseBeans+1, "coffeeDose should be 2")
}

/*
*
QUAND le technicien remet <x> gobelets
ALORS on ajoute <x> au stock de gobelets
*/
func Test_when_goblet_recharge_then_add_it_to_stock(t *testing.T) {
	var machine = CoffeeMachineBuilder()
	machine.SetGoblet(0)

	var baseGoblets = machine.goblet

	// When le technicien remet <x> gobelets
	machine.reloadGoblets(2)

	machine.insertCoin(0.40) // la machine marche toujours

	assert.Equal(t, machine.goblet, baseGoblets+1, "goblet should be 2")

}

/*
*
QUAND on met 40 cts ET 50cts
ALORS deux cafés coulent et 90cts sont encaissés.
*/
func Test_when_insert_40cts_and_50cts_then_two_coffee_are_served(t *testing.T) {
	var machine = CoffeeMachineBuilder()

	var baseMoney = machine.actualTotalMoney
	var baseCoffeeDose = machine.coffeeDose

	// Quand on met 40cts
	machine.insertCoin(0.40)
	machine.insertCoin(0.50)

	assert.Equal(t, machine.actualTotalMoney, baseMoney+0.90, "actualTotalMoney should be 0.90")

	// Alors deux cafés coulent
	assert.Equal(t, machine.coffeeDose, baseCoffeeDose-2, "coffeeDose should be 8")
}

/*
ETANT DONNE que le stock de café est de 60 - <x> doses
QUAND on ajoute <x> dose ALORS le stock de café est de 30 doses CAS 0, 1, 60
*/
func Test_when_coffee_recharge_60_then_add_it_to_stock(t *testing.T) {
	data := [3]int{0, 1, 60}
	for _, x := range data {

		if x > 60 {
			assert.Failf(t, "x should be less than 60", "x is %d", x)
		}

		var machine = CoffeeMachineBuilder()
		machine.SetCoffeeDose(60 - x)

		// When le technicien remet <x> café
		machine.reloadCoffee(x)

		assert.Equal(t, machine.coffeeDose, 60, "Beans are not refunded correctly")
	}
}

/*
ETANT DONNE que le stock de café est de maximum 60 doses
QUAND on ajoute plus de 60 dose
ALORS le stock de café est de 60 doses
*/
func Test_when_coffee_dose_60_max_then_add_it_to_stock(t *testing.T) {
	var machine = CoffeeMachineBuilder()
	machine.SetCoffeeDose(0)

	var baseBeans = machine.coffeeDose
	// When le technicien remet <x> café
	machine.reloadCoffee(61)

	assert.Equal(t, machine.coffeeDose, baseBeans+60, "Beans are not refunded correctly")
}

/*
ETANT DONNE que le stock de goblet est de 100 - <x>
QUAND on ajoute <x> dose ALORS le stock de café est de 100 doses CAS 0, 1, 60
*/
func Test_when_gobelet_recharge_100_then_add_it_to_stock(t *testing.T) {
	data := [3]int{0, 1, 100}
	for _, x := range data {

		if x > 100 {
			assert.Failf(t, "x should be less than 100", "x is %d", x)
		}

		var machine = CoffeeMachineBuilder()
		machine.SetGoblet(100 - x)

		// When le technicien remet <x> café
		machine.reloadGobelet(x)

		assert.Equal(t, machine.goblet, 100, "Beans are not refunded correctly")
	}
}

/*
ETANT DONNE que le stock de goblet est de maximum 60 doses
QUAND on ajoute plus de 100
ALORS le stock de café est de 100
*/
func Test_when_gobelet_100_max_then_add_it_to_stock(t *testing.T) {
	var machine = CoffeeMachineBuilder()
	machine.SetCoffeeDose(0)

	var baseBeans = machine.coffeeDose
	// When le technicien remet <x> café
	machine.reloadGobelet(101)

	assert.Equal(t, machine.goblet, baseBeans+100, "Beans are not refunded correctly")
}

func CoffeeMachineBuilder() *CoffeeMachine {
	return &CoffeeMachine{
		name:              "CoffeeMachine",
		coffeeDose:        10,
		goblet:            10,
		sugarDose:         10,
		sugarDoseInCoffee: 0,
		water:             true,
		moneyInserted:     0,
		actualTotalMoney:  0,
		cupDetected:       false,
	}
}
